# Palta

Palta is a modern PostCSS theme for Drupal 9.
It's a theme for theme editors and it aims to provide a nice and modern editing experience.

## Features

- Advanced theme editing with PostCSS, Stylelint and BrowserSync
- Clear cache and reload browser automatically when a twig template is updated (only with DDEV)
- ITCSS architecture
- Minimal default styles
- Responsive typography
- Drupal breakpoints integrated in PostCSS
- No grid system, it relies on CSS Grid

## Installation

Download and install as a normal Drupal theme, then install yarn dependencies.

```bash
yarn install
```

## Configuration

You can edit/remove configurations in .stylelintrc and postcss.config.js

## Author
Andrea Pistarini (aka febstarer) with lots of readings from [CSS tricks](https://css-tricks.com/), ideas from [Kraken](https://cferdinandi.github.io/kraken/)

## License
[MIT](https://choosealicense.com/licenses/mit/)