<?php

function palta_form_system_theme_settings_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id = NULL) {
  // Work-around for a core bug affecting admin themes. See issue #943212.
  if (isset($form_id)) {
    return;
  }

  $form['psettings'] = [
    '#type' => 'details',
    '#title' => 'Palta settings',
    '#open' => TRUE,
    '#weight' => -888
  ];

  $form['psettings']['fluid_layout'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Fluid layout'),
    '#default_value' => theme_get_setting('fluid_layout'),
    '#description'   => t("If false, content will be centered"),
  );

  $form['psettings']['back_to_top'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Back to top'),
    '#default_value' => theme_get_setting('back_to_top'),
    '#description'   => t("Adds a back to top widget when page is scrolled"),
  );

  $form['psettings']['convert_labels'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Convert labels to placeholder'),
    '#default_value' => theme_get_setting('convert_labels'),
    '#description'   => t(""),
  );
}