const config = {
  domain: "https://palta.test",
  reloadDelay: 2000,
  stylelint_options: {
    formatter: 'verbose',
    console: true,
    fix: true
  }
}
const gulp = require('gulp');
const postcss = require('gulp-postcss');
const stylelint = require('gulp-stylelint');
const rename = require('gulp-rename');
const browserSync = require('browser-sync');
const spawn = require('child_process').spawn;

const paths = {
  css: {
    src: './src/pcss/styles.pcss',
    destfile: 'styles.css',
    dest: './build/css',
    watch: './src/pcss/**/*.pcss',
  },
  js: {
    src: './src/js/**.js'
  },
  templates: {
    src: './templates/**/*.twig'
  }
}

const styles = () => {
  return gulp.src([paths.css.src], { sourcemaps: true })
    .pipe(stylelint({
      reporters: [
        config.stylelint_options
      ]
    }))
    .pipe(postcss())
    .on('error', function(errorInfo){  // if the error event is triggered, do something
      console.log(errorInfo.toString()); // show the error information
      this.emit('end'); // tell the gulp that the task is ended gracefully and resume
    })
    .pipe(rename({ extname: '.css' }))
    .pipe(gulp.dest(paths.css.dest, { sourcemaps: true }))
    .pipe(browserSync.stream());
}

const scripts = (cb) => {
  return cb;
}

// DDEV clear cache
const clearCache = (cb) => {
  spawn('ddev', ['drush', 'cr']);
  return cb;
}

const bsReload = () => {
  clearCache();
  return browserSync.reload();
};

// Static Server + watching scss/html files
const serve = () => {
  browserSync.init({
    proxy: config.domain,
    open: false,
    reloadDelay: config.reloadDelay
  })

  gulp.watch(paths.templates.src).on('change', bsReload);
  gulp.watch(paths.js.src, scripts).on('change', browserSync.reload);
  gulp.watch(paths.css.watch, styles);
}

exports.cr = gulp.series(clearCache);
exports.default = gulp.series(styles, serve);
