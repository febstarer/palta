module.exports = {
  plugins: {
    "postcss-import": {},
    "postcss-mixins": {},
    "postcss-simple-vars": {},
    "postcss-nested": {},
    "postcss-discard-comments": {},
    "rfs": {},
    "postcss-preset-env": {
      "browsers": "last 2 versions, not IE 11, not IE_Mob 11",
      "stage": 2
    },
    "postcss-sorting": {
      "order": [
        "custom-properties",
        "dollar-variables",
        "declarations",
        "at-rules",
        "rules",
      ],
      "properties-order": "alphabetical",
      "unspecified-properties-position": "bottom",
    },
    "postcss-drupal-breakpoints": {
      importFrom: "./palta.breakpoints.yml",
      themeName: "palta"
    },
  },
}
