/**
 * @file palta.js
 * 
 *
 */
(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Default config values will be updated immediatly
   */
  let config = {
    LANGUAGE: "it",
    THEME_NAME: "palta",
    WINDOW_WIDTH: "1280px",
    WINDOW_HEIGHT: "500px",
    SCROLL_TOP_DURATION: 300,
    SCROLL_TOP_OFFSET: 150,
    SCROLL_THRESHOLD: 5,
    RESIZE_THRESHOLD: 50,
    IS_MOBILE: false
  };

  $(document).ready(function(){
    setupConfig();
    setupBasic();
  });

  $(window).on('load', function() {
    if(drupalSettings.palta.back_to_top) {
      backToTop(config.SCROLL_TOP_DURATION, config.SCROLL_TOP_OFFSET);
    }
  });

  let setupConfig = function () {
    config.LANGUAGE = $('html').attr('lang');
    config.WINDOW_WIDTH = $(window).outerWidth();
    config.WINDOW_HEIGHT = $(window).outerHeight();

    if($('.mobile-menu').is(':visible')){
      config.IS_MOBILE = true;
      config.SCROLL_TOP_OFFSET = 50;
    }
  }

  let setupBasic = function(){

    // For all links with rel external, open link in new tab
    $('body').on('click', 'a[rel*="external"]', function(e){
      e.preventDefault();
      window.open($(this).attr('href'));
    });

    // disable links with specific class
    $('.link-disabled').on('click', function(e){
      e.preventDefault();
      return false;
    });

    $(window).on('scroll', function() {
      if ($(this).scrollTop() > config.SCROLL_TOP_OFFSET){
        $('body').addClass("scrolled");
        if(drupalSettings.palta.back_to_top) {
          $('body').addClass('palta-back-to-top-show');
        }
      } else{
        $('body').removeClass("scrolled");
        if(drupalSettings.palta.back_to_top) {
          $('body').removeClass('palta-back-to-top-show');
        }
      }
    });
  }

  Drupal.behaviors.palta = {
    attach: function (context, settings) {
      if(drupalSettings.palta.convert_labels) {
        convertLabelsToPlaceholders();
      }
    }
  };

}(jQuery, Drupal, drupalSettings));
