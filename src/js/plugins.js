/**
 * @file plugins.js
 * Global utilities.
 *
 */
let convertLabelsToPlaceholders, backToTop;

(function ($, Drupal, drupalSettings) {

  convertLabelsToPlaceholders = function(){
    $("form :input, form textarea, form select").each(function (i, elem) {
      var eId = $(elem).attr("id");

      if (eId && (label = $(elem).parents("form").find("label[for=" + eId + "]")).length === 1) {
        if ($(elem).prop('tagName') === 'SELECT') {
          // if it's a dropdown, change the first item ('All') to be the label
          $(elem).find('option:first').text('- ' + $(label).text() + ' -');
        } else {
          if ($(elem).is(':checkbox') || $(elem).is(':radio')) {
            // if it's a checkbox or radio, don't change anything
            return;
          }
          $(elem).attr("placeholder", $(label).text());
        }
        $(label).hide();
      }
    });
  }

  backToTop = function(SCROLL_TOP_DURATION){
    // Create a DOM element
    $('body').append('<a href="#" class="palta-back-to-top"><span>Back to top</span></a>');

    $('.palta-back-to-top').click(function(e){
      e.preventDefault();
      $('html, body').animate({scrollTop: 0}, SCROLL_TOP_DURATION);
      return false;
    })
  }
}(jQuery, Drupal, drupalSettings));
