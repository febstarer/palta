/**
 * @file palta.js
 * 
 *
 */
 (function ($, Drupal, drupalSettings) {

  'use strict';

  $(document).ready(function(){
    setupMobileMenu();
  });

  let setupMobileMenu = function () {
     // Creates the mobile menu element with a toggle button
     $('body').prepend('<div class="mobile-menu"><div class="inner"></div></div>');
     $('.mobile-menu').prepend($('<div class="menu-toggle"><button class="icon-toggle"></button></div>'));
     let $mobile_menu = $('.mobile-menu > .inner');
 
     // Choose the elements to put in the mobile menu
     let mobile_menu_elements = [
       '.block-system-branding-block',
       '.main-menu-block'];
 
     $.each(mobile_menu_elements, function(i, e){
       $mobile_menu.append($(e).clone());
     })
 
     $('.mobile-menu .menu-toggle button').click(function(e){
       e.preventDefault();
       $('body').toggleClass('mobile-menu-show');
     });
  }

}(jQuery, Drupal, drupalSettings));